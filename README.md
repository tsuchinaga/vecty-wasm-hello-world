### vecty wasm hello world

vectyを使ったspaでのはじめの一歩

なんでwasmかっていうと、gopherjsがmodに対応してなさそうやから。  
何かしらの方法で解決できるんかもやけど、現時点でwasmはgoに含まれてるんやし、wasm使ったほうが楽かなーって。

## 開発環境
* Windows 10 Pro
* Golang 1.12.9
* gopherjs/vecty latest(2019-08-18時点 v0.0.0-20190701174234-2b6fc20f8913)

## ビルド
`$ GOOS=js GOARCH=wasm go build -o app.wasm`

`$GOROOT/misc/wasm/wasm_exec.js`を使うので、コピーしてきてください

最近じゃ、GOROOTを明示的に宣言することもないみたいなので、installしたgoの近くにあるので探してください

## Webサーバ起動

`$ goexec 'http.ListenAndServe(":8080", http.FileServer(http.Dir(".")))'`

wasmを呼び出すFetchはhttpかhttpsでないと動かないみたいです。  
直接index.htmlをブラウザでファイルとしてみても、javascriptが動かないのでwasmの動作は見れません。  
なので簡単にサーバが立てられるgoexecを使っています。

`go get -u github.com/shurcooL/goexec`
