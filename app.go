package main

import (
	"github.com/gopherjs/vecty"
	"github.com/gopherjs/vecty/elem"
)

func main() {
	vecty.SetTitle("こんにちは べくてぃー")
	vecty.RenderBody(new(page))
}

type page struct {
	vecty.Core
}

func (p *page) Render() vecty.ComponentOrHTML {
	return elem.Body(vecty.Text("こんにちは べくてぃー"))
}
